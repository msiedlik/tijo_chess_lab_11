package pl.edu.pwsztar.domain.chess;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

public interface RulesOfGame {

    /**
     * Metoda zwraca true, tylko gdy przejscie z polozenia
     * (xStart, yStart) na (xEnd, yEnd) w jednym ruchu jest zgodne
     * z zasadami gry w szachy
     */
    boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd);

    @Component
    @Qualifier("Bishop")
    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }

            return Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart);
        }
    }

    @Component
    @Qualifier("Knight")
    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            final boolean isFirstCorrect = Math.abs(xStart - xEnd) == 1 && Math.abs(yStart - yEnd) == 2;
            final boolean isSecondCorrect = Math.abs(xStart - xEnd) == 2 && Math.abs(yStart - yEnd) == 1;

            return isFirstCorrect ^ isSecondCorrect;
        }
    }

    @Component
    @Qualifier("King")
    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }

            final int xLength = Math.abs(xStart - xEnd);
            final int yLength = Math.abs(yStart - yEnd);

            return (xLength == 0 || xLength == 1) && (yLength == 0 || yLength == 1);
        }
    }

    @Component
    @Qualifier("Queen")
    class Queen implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }

            final int xLength = Math.abs(xStart - xEnd);
            final int yLength = Math.abs(yStart - yEnd);

            final boolean isBishopMoveCorrect = xLength == yLength;
            final boolean isRookMoveCorrect = xLength == 0 || yLength == 0;

            return isBishopMoveCorrect || isRookMoveCorrect;
        }
    }

    @Component
    @Qualifier("Rock")
    class Rock implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }

            int xLength = Math.abs(xStart - xEnd);
            int yLength = Math.abs(yStart - yEnd);

            return xLength == 0 || yLength == 0;
        }
    }

    @Component
    @Qualifier("Pawn")
    class Pawn implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            return xStart == xEnd && yStart == yEnd - 1;
        }
    }
}
