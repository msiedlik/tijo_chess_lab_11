package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    final private RulesOfGame bishop;
    final private RulesOfGame knight;
    final private RulesOfGame king;
    final private RulesOfGame queen;
    final private RulesOfGame rock;
    final private RulesOfGame pawn;

    @Autowired
    public ChessServiceImpl(
            @Qualifier("Bishop") RulesOfGame bishop,
            @Qualifier("Knight") RulesOfGame knight,
            @Qualifier("King") RulesOfGame king,
            @Qualifier("Queen") RulesOfGame queen,
            @Qualifier("Rock") RulesOfGame rock,
            @Qualifier("Pawn") RulesOfGame pawn
    ) {
        this.bishop = bishop;
        this.knight = knight;
        this.king = king;
        this.queen = queen;
        this.rock = rock;
        this.pawn = pawn;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        final RulesOfGame rules = getRulesFromFigureType(figureMoveDto.getType());
        final String[] startPosition = figureMoveDto.getStart().split("_");
        final int xStart = parseBoardMarkToInt(startPosition[0]);
        final int yStart = parseBoardMarkToInt(startPosition[1]);

        final String[] endPosition = figureMoveDto.getDestination().split("_");
        final int xEnd = parseBoardMarkToInt(endPosition[0]);
        final int yEnd = parseBoardMarkToInt(endPosition[1]);
        return rules.isCorrectMove(xStart, yStart, xEnd, yEnd);
    }

    private RulesOfGame getRulesFromFigureType(FigureType type) {
        switch (type) {
            case KING:
                return king;
            case QUEEN:
                return queen;
            case ROCK:
                return rock;
            case BISHOP:
                return bishop;
            case KNIGHT:
                return knight;
            case PAWN:
                return pawn;
            default:
                throw new IllegalArgumentException("Invalid figure type " + type);
        }
    }

    private int parseBoardMarkToInt(String mark) {
        try {
            return Integer.parseInt(mark);
        } catch (Exception e) {
            return mapBoardMark(mark);
        }
    }

    private int mapBoardMark(String mark) {
        switch (mark) {
            case "a":
                return 1;
            case "b":
                return 2;
            case "c":
                return 3;
            case "d":
                return 4;
            case "e":
                return 5;
            case "f":
                return 6;
            case "g":
                return 7;
            case "h":
                return 8;
            default:
                throw new IllegalArgumentException("Mark should be in range a - h");
        }
    }
}
